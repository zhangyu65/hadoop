package com.zy.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;

public class HDFSApp {

    public static final String HDFS_PATH = "hdfs://47.97.202.10:9000";

    FileSystem fileSystem = null;
    Configuration configuration = null;


   @Test
   public void mkdir() throws IOException {
       fileSystem.mkdirs(new Path("/hadoopApi/text"));
   }

   @Test
   public void create() throws Exception{
        FSDataOutputStream out = fileSystem.create(new Path("/hadoopApi/text/a.txt"));
        out.write("hello hadoop".getBytes());
        out.flush();
        out.close();
   }

    @Test
    public void cat() throws Exception{
        FSDataInputStream in = fileSystem.open(new Path("/hadoopApi/text/b.txt"));
        IOUtils.copyBytes(in,System.out,1024);
        in.close();
    }

    @Test
    public void rename() throws Exception{
       Path old = new Path("/hadoopApi/text/a.txt");
       Path newPath = new Path("/hadoopApi/text/b.txt");
       fileSystem.rename(old, newPath);
    }

    @Test
    public void copyFromLocalFile() throws Exception{
       Path localPath = new Path("E:\\BaiduNetdiskDownload\\资料\\大数据\\文档资料\\day02\\awk详解.txt");
       Path hdfsPath = new Path("/hadoopApi/text");
       fileSystem.copyFromLocalFile(localPath, hdfsPath);
    }

    @Test
    public void listFiles() throws Exception{
        RemoteIterator ri = fileSystem.listFiles(new Path("/hadoopApi"), false);
        if (ri.hasNext()){
            System.out.println(ri.toString());
        }
    }

    @Before
    public void setUp() throws Exception{
        configuration = new Configuration();
        configuration.set("dfs.client.use.datanode.hostname","true");
        fileSystem = FileSystem.get(new URI(HDFS_PATH), configuration, "hadoop");
    }

    @After
    public void tearDown() throws Exception{
        configuration = null;
        fileSystem = null;
    }
}
